#!/bin/bash

UNITY_PATH=/home/vxdcxw/Unity/Hub/Editor/2020.3.2f1/Editor/Unity
BUILD_TARGET=Linux64 # Linux64, WebGL
BUILD_METHOD=BuildServerDevelopment # BuildServerProduction, BuildWebGL

$UNITY_PATH -quit -batchmode -buildTarget $BUILD_TARGET -nographics -projectPath . -executeMethod LostSouls.Editor.Build.AutoBuild.$BUILD_METHOD -logfile ./Logs/build.log
