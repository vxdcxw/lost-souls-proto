# Lost Souls

Multiplayer labyrinth game made during GGJ 2021 on theme "Lost and Found"

## Project

Unity version 2020.3.2f1

## Build

**Building server requires Unity `Linux Build Support` module**

**Building client requires Unity `WebGL Build support` module**

### Basic configuration

* Select a Build environment in the `ConfigurationManager` object (default: Development)
* Find the `Build` menu and select desired target

### Advanced configuration

It is possible to build from Unty build player options for more control on build options

## Run Server

### Without Docker

Copy `example.cfg` to a place accessible to the application and edit variables to your needs

Run the server with following arguments

```
./lost-souls.x86_64 --name local --logs /tmp/logs --config ./server.cfg
```

### With Docker Compose

Copy `example.env` to `.env` in the project root folder and edit variables to your needs

Build & run both client & server

```
docker-compose up --build
```

Visit http://localhost:6954/ to run the WebGL application

Enjoy!
