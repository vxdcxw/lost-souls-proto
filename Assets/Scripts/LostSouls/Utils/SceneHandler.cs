using LostSouls.Network;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LostSouls.Utils
{

    public class SceneHandler : MonoBehaviour
    {
        public int PreloadSceneBuildIndex;

        private void Awake()
        {
#if UNITY_EDITOR
            if (CustomNetworkManager.Instance != null)
            {
                return;
            }

            SceneManager.LoadScene(PreloadSceneBuildIndex);
#endif
        }
    }

}
