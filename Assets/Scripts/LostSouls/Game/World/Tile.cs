using LostSouls.Game.Graphics;
using LostSouls.Generators;
using UnityEngine;
using UnityEngine.Networking;

namespace LostSouls.Game.World
{

    public class Tile : NetworkBehaviour
    {
        [SyncVar] private Directions _directions;
        [SyncVar] private int _directionCount;
        [SyncVar] public int PatchId;

        public override void OnStartClient()
        {
            LoadPrefab();

            if (PatchId >= 0)
            {
                var go = new GameObject("Floor Patch");
                var spriteRenderer = go.AddComponent<SpriteRenderer>();

                go.AddComponent<Billboard>();
                go.transform.SetParent(transform, false);

                spriteRenderer.sprite = PrefabManager.Instance.FloorSprites[PatchId];
            }
        }

        public void SetOrientation(MazeGenerator.Flag Flags)
        {

            if (HasFlag(Flags, MazeGenerator.Flag.North))
            {
                _directions |= Directions.Up;
                _directionCount++;
            }

            if (HasFlag(Flags, MazeGenerator.Flag.South))
            {
                _directions |= Directions.Down;
                _directionCount++;
            }

            if (HasFlag(Flags, MazeGenerator.Flag.East))
            {
                _directions |= Directions.Right;
                _directionCount++;
            }

            if (HasFlag(Flags, MazeGenerator.Flag.West))
            {
                _directions |= Directions.Left;
                _directionCount++;
            }

            if (_directions == Directions.Right || _directions == Directions.StraightHorizontal || _directions == Directions.CornerUpRight || _directions == Directions.IntersectionNoLeft)
            {
                transform.Rotate(new Vector3(0, 0, 90));
            }
            else if (_directions == Directions.Up || _directions == Directions.CornerUpLeft || _directions == Directions.IntersectionNoDown)
            {
                transform.Rotate(new Vector3(0, 0, 180));
            }
            else if (_directions == Directions.Left || _directions == Directions.CornerDownLeft || _directions == Directions.IntersectionNoRight)
            {
                transform.Rotate(new Vector3(0, 0, 270));
            }
        }

        private void LoadPrefab()
        {
            if (_directionCount <= 1)
            {
                Instantiate(PrefabManager.Instance.Prefabs.DeadEnd, transform);
            }
            else if (_directionCount <= 2)
            {
                if (_directions.HasFlag(Directions.StraightVertical) || _directions.HasFlag(Directions.StraightHorizontal))
                {
                    Instantiate(PrefabManager.Instance.Prefabs.Straight, transform);
                }
                else
                {
                    Instantiate(PrefabManager.Instance.Prefabs.Corner, transform);
                }
            }
            else if (_directionCount <= 3)
            {
                Instantiate(PrefabManager.Instance.Prefabs.ThreeWays, transform);
            }
            else if (_directionCount <= 4)
            {
                Instantiate(PrefabManager.Instance.Prefabs.FourWays, transform);
            }
        }

        private static bool HasFlag(MazeGenerator.Flag flags, MazeGenerator.Flag flag)
        {
            return (flags & flag) != 0;
        }

        [System.Flags]
        private enum Directions
        {
            None = 0x0,
            Up = 0x01,
            Down = 0x02,
            Right = 0x04,
            Left = 0x08,

            StraightVertical = 0x03,
            StraightHorizontal = 0x0c,
            CornerUpRight = 0x05,
            CornerDownRight = 0x06,
            CornerUpLeft = 0x09,
            CornerDownLeft = 0x0a,
            IntersectionNoLeft = 0x7,
            IntersectionNoRight = 0xb,
            IntersectionNoDown = 0xd,
            IntersectionNoUp = 0xe,
            All = 0x0f,
        }
    }

}
