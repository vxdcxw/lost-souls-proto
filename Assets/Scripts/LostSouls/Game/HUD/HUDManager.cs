using Community;
using LostSouls.Network;
using UnityEngine;
using UnityEngine.UI;

namespace LostSouls.Game.HUD
{
    public class HUDManager : MonoSingleton<HUDManager>
    {
        public Image ItemSlotImage;
        public Text ItemHintLabel;

        public Text ConnectedPlayersLabel;
        public Text TeamSizeLabel;
        public Text TimerLabel;

        public GameObject EndGameDialog;
        public string MessageVictory;
        public string MessageDefeat;

        public GameObject MessageDialog;
        [Multiline] public string MessageWaitForPlayers;
        [Multiline] public string MessageLastPlayer;
        [Multiline] public string MessageGameIsRunning;

        public GameObject CountDownDialog;

        private Text _messageLabel;
        private Text _countDownLabel;

        protected override void Init()
        {
#if UNITY_SERVER
            enabled = false;

            return;
#endif
        }

        private void Start()
        {
            _messageLabel = MessageDialog.GetComponentInChildren<Text>(true);
            _countDownLabel = CountDownDialog.GetComponentInChildren<Text>(true);
        }

        public static void UpdateActiveConnections(int connections)
        {
            Instance.ConnectedPlayersLabel.text = connections.ToString();
        }

        public static void UpdateCountdown(int countdown)
        {
            if (countdown < 1)
                Instance.CountDownDialog.SetActive(false);
            else
            {
                Instance.MessageDialog.SetActive(false);
                Instance.CountDownDialog.SetActive(true);
                Instance._countDownLabel.text = countdown.ToString();
            }
        }

        public static void UpdateTimer(int timer)
        {
            Instance.TimerLabel.text = timer.ToString();
        }

        public static void UpdateTeamSize(int teamSize)
        {
            Instance.TeamSizeLabel.text = (NetworkData.Instance.ActiveConnections - teamSize).ToString();
        }

        public static void DisplayEndGameDialog(bool victory)
        {
            Instance.EndGameDialog.SetActive(true);
            Instance.EndGameDialog.GetComponentInChildren<Text>().text = victory ? Instance.MessageVictory : Instance.MessageDefeat;
        }

        public static void SetItemSlotImage(Sprite image)
        {
            Instance.ItemSlotImage.sprite = image;
        }

        public static void DisplayItemHint(bool hide = false)
        {
            Instance.ItemHintLabel.gameObject.SetActive(!hide);
        }

        public static void DisplayWaitMessage(bool active = true)
        {
            Instance._messageLabel.text = Instance.MessageWaitForPlayers;
            Instance.MessageDialog.SetActive(active);
            Instance.CountDownDialog.SetActive(!active);
        }

        public static void DisplayLastPlayerMessage(bool active = true)
        {
            Instance._messageLabel.text = Instance.MessageLastPlayer;
            Instance.MessageDialog.SetActive(active);
        }

        public static void DisplaySpectatorMessage(bool active = true)
        {
            Instance._messageLabel.text = Instance.MessageGameIsRunning;
            Instance.MessageDialog.SetActive(active);
        }
    }
}
