using LostSouls.Game.Graphics;
using LostSouls.Game.Items;
using UnityEngine;
using UnityEngine.Networking;

namespace LostSouls.Game.GameplayElements
{
    public class FloorItem : NetworkBehaviour
    {
        public Sprite ArrowSprite;
        public Sprite CoinSprite;
        [SyncVar] public Item.Type Type;

        private SpriteRenderer _renderer;
        private bool _isDirty;

        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _isDirty = true;
        }

        private void Update()
        {
            if (!_isDirty)
                return;

            SetSprite();

            _isDirty = false;
        }

        private void SetSprite()
        {
            switch (Type)
            {
                case Item.Type.Arrow:
                    _renderer.sprite = ArrowSprite;
                    break;

                case Item.Type.Coin:
                    _renderer.sprite = CoinSprite;
                    gameObject.AddComponent<Billboard>();
                    break;

                default:
                    _renderer.sprite = null;
                    break;
            }
        }

        public void SetType(Item.Type type)
        {
            Type = type;
        }
    }
}
