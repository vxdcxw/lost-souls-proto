using LostSouls.Game.Items;
using LostSouls.Game.Player;
using UnityEngine;
using UnityEngine.Networking;
using NetworkPlayer = LostSouls.Game.Player.NetworkPlayer;

namespace LostSouls.Game.GameplayElements
{
    [RequireComponent(typeof(AudioSource), typeof(SpriteRenderer))]
    public class Treasure : NetworkBehaviour
    {
        public AudioClip ClipOpen;
        public Sprite SpriteOpen;

        private AudioSource _source;
        private SpriteRenderer _renderer;
        private BoxCollider2D _collider;

        private Item Item { get; set; }

        [field: SyncVar(hook = nameof(Open))] private bool IsOpen { get; set; }

        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _source = GetComponent<AudioSource>();
            _collider = GetComponent<BoxCollider2D>();
        }

        private void Start()
        {
            if (isServer)
                Item = PrefabManager.RandomItem;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (IsOpen || !other.gameObject.CompareTag("Player"))
                return;

            if (isServer)
            {
                other.GetComponent<Inventory>().ServerSetItem(Item.SpriteType);

                IsOpen = true;
            }
            else if (other.GetComponent<NetworkPlayer>().isLocalPlayer)
            {
                _source.PlayOneShot(ClipOpen);
            }
        }

        public void Open(bool isOpen)
        {
            if (!isOpen)
                return;

            _renderer.sprite = SpriteOpen;
            _collider.enabled = false;
        }
    }
}
