using System;
using System.Collections;
using JetBrains.Annotations;
using LostSouls.Audio;
using LostSouls.Game.HUD;
using UnityEngine;
using UnityEngine.Events;
using NetworkPlayer = LostSouls.Game.Player.NetworkPlayer;

namespace LostSouls.Game.Graphics
{
    // Rename CharacterAnimator
    [RequireComponent(typeof(SpriteRenderer))]
    public class CharacterSprite : MonoBehaviour
    {
        private static readonly int LookOrientation = Animator.StringToHash("LookOrientation");
        private static readonly int IsIdle = Animator.StringToHash("IsIdle");

        public event UnityAction<bool> SetPlayerVisibility;

        public float VisibilityDelay = 0.5f;
        public float PlayerCloseRadius = 1.0f;
        public float SightRadius = 2.0f;

        private SpriteRenderer _spriteRenderer;
        private Transform _mainCamera;
        private Animator _animator;
        private AudioEvent _audioEvent;
        private Rigidbody2D _rigidbody;
        private CircleCollider2D _collider;
        private LayerMask _playersMask;
        private LayerMask _wallsMask;
        // TODO - Set colliders count according to number of players
        private readonly Collider2D[] _colliders = new Collider2D[20];
        private float _visibilityElapsedTime;
        private Coroutine _visibilityCoroutine;

        private bool _isLocalPlayer;

        private void Awake()
        {
            _playersMask = LayerMask.GetMask("Players");
            _wallsMask = LayerMask.GetMask("Walls");
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _animator = GetComponent<Animator>();
            _audioEvent = GetComponentInParent<AudioEvent>();
            _rigidbody = GetComponentInParent<Rigidbody2D>();
            _collider = GetComponentInParent<CircleCollider2D>();
            _isLocalPlayer = GetComponentInParent<NetworkPlayer>().isLocalPlayer;
        }

        private void Start()
        {
            _mainCamera = Camera.main.transform;
        }

        private void Update()
        {
            if (!_spriteRenderer.enabled)
                return;

            float cameraX;
            float cameraY;

            var orientation = 0;
            var velocity = _rigidbody.velocity;

            if (Math.Abs(_mainCamera.localRotation.eulerAngles.z) < 0.1f)
            {
                cameraX = velocity.x;
                cameraY = velocity.y;
            }
            else if (Math.Abs(_mainCamera.localRotation.eulerAngles.z) < 90.1f)
            {
                cameraX = velocity.y;
                cameraY = -velocity.x;
            }
            else if (Math.Abs(_mainCamera.localRotation.eulerAngles.z) < 180.1f)
            {
                cameraX = -velocity.x;
                cameraY = -velocity.y;
            }
            else
            {
                cameraX = -velocity.y;
                cameraY = velocity.x;
            }

            var idle = Mathf.Abs(cameraY) < 0.01f && Mathf.Abs(cameraX) < 0.01f;
            var isVertical = Mathf.Abs(cameraY) > Mathf.Abs(cameraX);

            if (Mathf.Abs(cameraY) > 0 || Mathf.Abs(cameraX) > 0)
            {
                if (isVertical)
                    orientation = cameraY > 0 ? 1 : 0;
                else
                    orientation = 2;

                _animator.SetInteger(LookOrientation, orientation);
            }

            _animator.SetBool(IsIdle, idle);

            if (orientation == 2)
                _spriteRenderer.flipX = cameraX > 0f;
        }

        private void FixedUpdate()
        {
            if (!_isLocalPlayer)
                return;

            CheckPlayersVisibility();
        }

        public void Hide()
        {
            _spriteRenderer.enabled = false;
            SetPlayerVisibility?.Invoke(false);
        }

        private IEnumerator SetVisibility()
        {
            _spriteRenderer.enabled = true;
            SetPlayerVisibility?.Invoke(true);

            yield return new WaitForSeconds(VisibilityDelay);

            _spriteRenderer.enabled = false;
            SetPlayerVisibility?.Invoke(false);
        }

        // TODO - Detecting a player is visible or not should be done server-side
        private void CheckPlayersVisibility()
        {
            var teamSize = 1;
            var playerTransform = _collider.transform;

            Physics2D.OverlapCircleNonAlloc(playerTransform.position, SightRadius, _colliders, _playersMask);

            foreach (var c in _colliders)
            {
                if (c == null)
                    break;

                if (c.transform == playerTransform)
                    continue;

                var distance = Vector2.Distance(playerTransform.position, c.transform.position);

                if (distance > PlayerCloseRadius)
                    continue;

                var direction = (c.transform.position - playerTransform.position);
                var hit = Physics2D.Raycast(playerTransform.position, direction, distance, _wallsMask);

                if (hit.transform != null)
                    continue;

                if (_visibilityCoroutine != null)
                    StopCoroutine(_visibilityCoroutine);

                _visibilityCoroutine = StartCoroutine(c.transform.GetComponentInChildren<CharacterSprite>().SetVisibility());

                teamSize++;
            }

            HUDManager.UpdateTeamSize(teamSize);
        }

        [UsedImplicitly]
        public void FootStepEvent()
        {
            if (!_isLocalPlayer)
                return;

            _audioEvent.PlaySFX();
        }
    }
}
