using System.Collections;
using UnityEngine;

namespace LostSouls.Game.Graphics
{

    public class Billboard : MonoBehaviour
    {
        private Transform _mainCamera;

        private void Start()
        {
            if (Camera.main == null)
                return;

            _mainCamera = Camera.main.transform;

            StartCoroutine(ChangeOrientationOnNextFrame());
        }

        private IEnumerator ChangeOrientationOnNextFrame()
        {
            yield return null;

            transform.rotation = _mainCamera.rotation;

            yield return new WaitForSeconds(5);

            transform.rotation = _mainCamera.rotation;

            yield return new WaitForSeconds(30);

            transform.rotation = _mainCamera.rotation;
        }
    }

}
