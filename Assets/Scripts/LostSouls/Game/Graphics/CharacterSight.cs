using System.Collections;
using LostSouls.Network;
using UnityEngine;
using NetworkPlayer = LostSouls.Game.Player.NetworkPlayer;

namespace LostSouls.Game.Graphics
{
    public class CharacterSight : MonoBehaviour
    {
        public float SightRadius = 1.0f;
        public float SightRadiusMinimum = 0.3f;
        public float HurryUpShrinkDuration = 1.0f;
        public Transform SightShadow;
        public Transform SightMask;

        private NetworkPlayer _networkPlayer;
        private Coroutine _scaleCoroutine;

        private void Awake()
        {
            _networkPlayer = GetComponentInParent<NetworkPlayer>();

            if (_networkPlayer.isLocalPlayer)
            {
                NetworkData.Instance.OnGameHurryUp += () => StartScaleCoroutine(SightRadiusMinimum, HurryUpShrinkDuration);
                NetworkData.Instance.OnGameEnd += (victory) => StartScaleCoroutine(victory ? 20 : 0, 10.0f);

                SightMask.localScale = Vector3.zero;
            }

            GetComponent<CharacterSprite>().SetPlayerVisibility += SightMask.gameObject.SetActive;

            SightMask.gameObject.SetActive(false);
        }

        private void Start()
        {
            SightMask.gameObject.SetActive(_networkPlayer.isLocalPlayer);
            SightShadow.gameObject.SetActive(_networkPlayer.isLocalPlayer);

            if (_networkPlayer.isLocalPlayer)
                StartScaleCoroutine(SightRadius, NetworkData.Instance.StartCountdown);
        }

        private void StartScaleCoroutine(float toScale, float duration)
        {
            if (_scaleCoroutine != null)
                StopCoroutine(_scaleCoroutine);

            _scaleCoroutine = StartCoroutine(ScaleSight(toScale, duration));
        }

        private IEnumerator ScaleSight(float toScale, float duration)
        {
            var initialScale = SightMask.localScale.x;
            var start = Time.time;
            var end = start + duration;

            while (Time.time < end)
            {
                var scale = Mathf.SmoothStep(initialScale, toScale, (Time.time - start) / duration);

                SightMask.localScale = Vector3.one * scale;

                yield return null;
            }

            SightMask.localScale = Vector3.one * toScale;
        }
    }
}
