using System.Collections.Generic;
using Community;
using LostSouls.Game.Items;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LostSouls.Game
{
    public class PrefabManager : MonoSingleton<PrefabManager>
    {
        public GameObject TilePrefab;
        public GameObject FloorItemPrefab;
        public GameObject TreasurePrefab;
        public List<Sprite> FloorSprites;
        public List<Item> ItemPrefabs;
        [SerializeField] public TilePrefabs Prefabs;

        public static Item RandomItem => Instance.ItemPrefabs[Random.Range(0, Instance.ItemPrefabs.Count)];

        [System.Serializable]
        public class TilePrefabs
        {
            public GameObject DeadEnd;
            public GameObject Straight;
            public GameObject Corner;
            public GameObject ThreeWays;
            public GameObject FourWays;
        }
    }
}
