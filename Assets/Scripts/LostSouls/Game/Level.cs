using System;
using System.Collections.Generic;
using LostSouls.Game.World;
using LostSouls.Generators;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using LogLevel = LostSouls.Server.Logger.Level;

namespace LostSouls.Game
{
    public class Level
    {
        private readonly LogAction _log;
        private readonly bool _server;

        private readonly Transform _parent;

        private readonly int _chestsCount;
        private readonly int _minWidth;
        private readonly int _maxWidth;
        private readonly int _minHeight;
        private readonly int _maxHeight;
        private readonly int _minPathLength;
        private readonly int _maxPathLength;

        private readonly int _spawnPoints;

        private readonly uint _seed;

        private static Random.State _seedGenerator;
        private static uint _seedGeneratorSeed = 1837;
        private static bool _seedGeneratorInitialized = false;

        private List<GameObject> _tiles;

        public int Width { get; private set; }
        public int Height { get; private set; }
        public int PathLength { get; private set; }
        public int ChestsCount => _chestsCount;
        public uint Seed => _seed;

        public Level(Vector2Int width, Vector2Int height, Vector2Int pathLength, int chestsCount, int spawnPoints, Transform parent, uint seed = 0, bool server = false, LogAction log = null)
        {
            _log = log;
            _server = server;

            _parent = parent;
            _minWidth = width.x;
            _maxWidth = width.y;
            _minHeight = height.x;
            _maxHeight = height.y;
            _minPathLength = pathLength.x;
            _maxPathLength = pathLength.y;
            _chestsCount = chestsCount;
            _spawnPoints = spawnPoints;

            if (seed != 0)
            {
                _seedGeneratorSeed = (uint) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                _seed = GenerateSeed();
            }
        }

        private static uint GenerateSeed()
        {
            // Store previous seed
            var temp = Random.state;

            // Initialize generator state if needed
            if (!_seedGeneratorInitialized)
            {
                Random.InitState((int) _seedGeneratorSeed);
                _seedGenerator = Random.state;
                _seedGeneratorInitialized = true;
            }

            // Set our generator state to the seed generator
            Random.state = _seedGenerator;
            // Generate a new seed
            var generatedSeed = (uint) Random.Range(int.MinValue, int.MaxValue);
            // Cache the new generator state
            _seedGenerator = Random.state;
            // Set the original state back so that normal random generation can continue where it left off
            Random.state = temp;

            return generatedSeed;
        }

        public void Generate()
        {
            Random.InitState((int) _seed);

            Width = Random.Range(_minWidth, _maxWidth + 1);
            Height = Random.Range(_minHeight, _maxHeight + 1);
            PathLength = Random.Range(_minPathLength, _maxPathLength + 1);
            var maze = new MazeGenerator(Width, Height, PathLength);
            var map = maze.Generate((int) _seed);

            _log($"New map (Seed:{_seed}, Width:{Width}, Height:{Height}, Path Length:{PathLength}, Chests count:{_chestsCount})", (int) LogLevel.Info);

            if (map == null)
            {
                // TODO - a bit harsh, should instead send back to title screen
                Application.Quit();

                return;
            }

            _tiles = new List<GameObject>();

            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    var idx = j + (i * Width);
                    var tile = Object.Instantiate(PrefabManager.Instance.TilePrefab, _parent);
                    var tileComponent = tile.GetComponent<Tile>();

                    tile.transform.localPosition = new Vector3(j * 4 - (Width / 2), -(i * 4 - (Height / 2)), 0);
                    tileComponent.SetOrientation(map[idx]);

                    if (Random.Range(0f, 1.0f) > 0.62f)
                        tileComponent.PatchId = Random.Range(0, PrefabManager.Instance.FloorSprites.Count);
                    else
                        tileComponent.PatchId = -1;

                    if (_server)
                        NetworkServer.Spawn(tile);

                    _tiles.Add(tile);
                }
            }

            SetPlayersStartPosition();
            SpawnChests();
        }

        private void SetPlayersStartPosition()
        {
            for (var i = 0; i < _spawnPoints; i++)
            {
                var tile = _tiles[Random.Range(0, _tiles.Count)];
                var tilePosition = tile.transform.position;
                var startPosition = new GameObject("PlayerSpawnPoint", typeof(NetworkStartPosition));

                startPosition.transform.position = new Vector3(tilePosition.x, tilePosition.y, -0.05f);

                _tiles.Remove(tile);

                _log($"New spawn point ({startPosition.transform.position.x}, {startPosition.transform.position.y})", (int) LogLevel.Debug);
            }
        }

        private void SpawnChests()
        {
            for (var i = 0; i < _chestsCount; i++)
            {
                var tile = _tiles[Random.Range(0, _tiles.Count)];
                var tilePosition = tile.transform.position;
                var treasure = Object.Instantiate(PrefabManager.Instance.TreasurePrefab);

                treasure.transform.position = new Vector3(tilePosition.x, tilePosition.y, -0.05f);

                if (_server)
                    NetworkServer.Spawn(treasure);

                _tiles.Remove(tile);
            }
        }

        public delegate void LogAction(string message, int level = 0, string filename = null, bool raw = false, bool sendNotification = false);
    }
}
