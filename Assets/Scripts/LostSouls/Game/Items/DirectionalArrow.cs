using UnityEngine;

namespace LostSouls.Game.Items
{
    [CreateAssetMenu(fileName = "ChestItem", menuName = "ScriptableObjects/DirectionalArrow", order = 1)]
    public class DirectionalArrow : Item
    {
        public override Quaternion GetRotation(Vector2 direction)
        {
            Quaternion rot;

            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                rot = direction.x > 0 ? Quaternion.Euler(0f, 0f, -90.0f) : Quaternion.Euler(0f, 0f, 90.0f);
            else
                rot = Quaternion.Euler(0f, 0f, direction.y > 0 ? 0.0f : 180.0f);

            return rot;
        }
    }
}
