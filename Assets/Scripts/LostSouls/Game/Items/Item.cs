using UnityEngine;

namespace LostSouls.Game.Items
{
    public abstract class Item : ScriptableObject
    {
        public Sprite Image;
        public Type SpriteType;
        public AudioClip Clip;
        public int Amount;

        public abstract Quaternion GetRotation(Vector2 direction);

        public enum Type
        {
            None,
            Arrow,
            Coin,
        }
    }
}
