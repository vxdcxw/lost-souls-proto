using UnityEngine;

namespace LostSouls.Game.Items
{
    [CreateAssetMenu(fileName = "ChestItem", menuName = "ScriptableObjects/CoinsPurse", order = 1)]
    public class CoinsPurse : Item
    {
        public override Quaternion GetRotation(Vector2 direction)
        {
            return Camera.main.transform.rotation;
        }
    }
}
