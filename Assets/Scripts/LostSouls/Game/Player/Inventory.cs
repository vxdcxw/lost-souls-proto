using System;
using LostSouls.Game.GameplayElements;
using LostSouls.Game.HUD;
using LostSouls.Game.Items;
using UnityEngine;
using UnityEngine.Networking;

namespace LostSouls.Game.Player
{
    public class Inventory : NetworkBehaviour
    {
        [SerializeField] private Sprite EmptyItemSlotImage;
        [SerializeField] private DirectionalArrow ArrowItem;
        [SerializeField] private CoinsPurse PurseItem;

        private Item _item;
        private AudioSource _audioSource;

        [field: SyncVar(hook = nameof(ClientSetItem))] private Item.Type ItemType { get; set; }

        private void Start()
        {
            _audioSource = GetComponentInChildren<AudioSource>();
        }

        public void ClientUseItem(Vector2 normalizedVelocity)
        {
            if (_item == null)
                return;

            _audioSource.PlayOneShot(_item.Clip);

            CmdUseItem(_item.GetRotation(normalizedVelocity));
        }

        private void ClientSetItem(Item.Type type)
        {
            if (type == Item.Type.None)
            {
                _item = null;

                if (GetComponent<NetworkPlayer>().isLocalPlayer)
                {
                    HUDManager.SetItemSlotImage(EmptyItemSlotImage);
                    HUDManager.DisplayItemHint(true);
                }
            }
            else
            {
                _item = Instantiate(GetItemByType(type));

                if (GetComponent<NetworkPlayer>().isLocalPlayer)
                {
                    HUDManager.SetItemSlotImage(_item.Image);
                    HUDManager.DisplayItemHint();
                }
            }
        }

        [Command]
        private void CmdUseItem(Quaternion rotation)
        {
            if (_item.Amount <= 0)
                return;

            ServerSpawnItem(transform.position, rotation, _item.SpriteType);

            _item.Amount--;

            if (_item.Amount <= 0)
                ItemType = Item.Type.None;
        }

        private void ServerSpawnItem(Vector3 pos, Quaternion rot, Item.Type itemType)
        {
            var go = Instantiate(PrefabManager.Instance.FloorItemPrefab);
            var floorItem = go.GetComponent<FloorItem>();

            go.transform.position = pos;
            go.transform.rotation = rot;

            floorItem.SetType(itemType);

            NetworkServer.Spawn(go);
        }

        public void ServerSetItem(Item.Type type)
        {
            _item = Instantiate(GetItemByType(type));

            ItemType = type;
        }

        private Item GetItemByType(Item.Type type)
        {
            return type switch
            {
                Item.Type.None => null,
                Item.Type.Arrow => ArrowItem,
                Item.Type.Coin => PurseItem,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
}
