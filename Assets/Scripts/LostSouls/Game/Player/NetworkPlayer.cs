using LostSouls.Audio;
using LostSouls.Game.Graphics;
using LostSouls.Game.HUD;
using LostSouls.Network;
using UnityEngine;
using UnityEngine.Networking;

namespace LostSouls.Game.Player
{
    public class NetworkPlayer : NetworkBehaviour
    {
        public static bool IsConnected;

        private void Start()
        {
            if (!isLocalPlayer)
                Destroy(GetComponent<PlayerController>());

            if (!isServer)
            {
                Instantiate(Client.PrefabManager.Instance.PlayerPuppetPrefab, transform);

                // Force transform to move to update network position
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.01f, transform.position.z);

                if (!isLocalPlayer)
                {
                    GetComponentInChildren<CharacterSprite>().Hide();
                }
                else
                {
                    // The PlayerController is enabled by default to prevent awake/start events
                    GetComponent<PlayerController>().enabled = true;

                    NetworkData.Instance.OnGameHurryUp += InGameMusic.Instance.PlayHurryUpTheme;
                    NetworkData.Instance.OnGameEnd += TriggerEndGameEvents;

                    InGameMusic.Instance.PlayMainTheme();
                }

                IsConnected = true;
            }
        }

        private void TriggerEndGameEvents(bool victory)
        {
            HUDManager.DisplayEndGameDialog(victory);
            InGameMusic.Instance.PlayEndGameTheme(victory);
            AudioSpawnManager.Stop();
        }
    }
}
