using System.Collections;
using LostSouls.Network;
using UnityEngine;
using UnityEngine.U2D;
using Random = UnityEngine.Random;

namespace LostSouls.Game.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float Speed = 4.0f;

        private Transform _mainCamera;
        private Rigidbody2D _rigidbody;
        private Inventory _inventory;
        private bool _active;
        private PixelPerfectCamera _ppCamera;

        private void Awake()
        {
            _active = false;
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            _mainCamera = Camera.main.transform;
            _ppCamera = _mainCamera.GetComponent<PixelPerfectCamera>();

            _mainCamera.parent = transform;
            _mainCamera.localPosition = new Vector3(0, 0, _mainCamera.localPosition.z);
            _mainCamera.localRotation = Quaternion.Euler(0, 0, Random.Range(0, 4) * 90);
        }

        private void OnEnable()
        {
            // Lazy load components spawned on client
            _inventory = GetComponentInChildren<Inventory>();

            NetworkData.Instance.OnGameStart += () => _active = true;
            NetworkData.Instance.OnGameEnd += (victory) =>
            {
                _active = false;

                StartCoroutine(RollBackCamera(victory));
            };
        }

        private void Update()
        {
            if (Input.GetButtonUp("Cancel"))
                CustomNetworkManager.Instance.StopClient();

            if (!_active)
            {
                _rigidbody.velocity *= 0.96f;

                return;
            }

            if (Input.GetButtonUp("Action"))
                _inventory.ClientUseItem(_rigidbody.velocity.normalized);

            var inputs = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            inputs = _mainCamera.localRotation * inputs;

            _rigidbody.velocity = Vector2.ClampMagnitude(inputs, 1f) * Speed;
        }

        private IEnumerator RollBackCamera(bool victory)
        {
            var start = Time.time;
            var originalPPU = _ppCamera.assetsPPU;
            var from = _mainCamera.localRotation.eulerAngles.z;

            while (true)
            {
                var ppu = (int) Mathf.SmoothStep(originalPPU, 4, (Time.time - start) / 5);

                _ppCamera.assetsPPU = ppu;

                if (!victory)
                {
                    var z = Mathf.SmoothStep(from, from + 360 * 3, (Time.time - start) / 5);

                    _mainCamera.localRotation = Quaternion.Euler(0, 0, z);
                }

                yield return null;
            }

            yield return null;
        }
    }
}
