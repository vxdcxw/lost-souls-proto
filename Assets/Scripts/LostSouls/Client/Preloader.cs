using UnityEngine;
using UnityEngine.SceneManagement;

namespace LostSouls.Client
{

    public class Preloader : MonoBehaviour
    {
        public GameObject CameraPrefab;

        private Transform _instantiated;

        private void Awake()
        {
            SceneManager.sceneLoaded += OnSceneLoad;
        }

        private void OnSceneLoad(Scene scene, LoadSceneMode sceneMode)
        {
            if (_instantiated == null && sceneMode == LoadSceneMode.Single)
            {
                _instantiated = Instantiate(CameraPrefab).transform;
            }
        }
    }

}
