using Community;
using UnityEngine;

namespace LostSouls.Client
{
    public class PrefabManager : MonoSingleton<PrefabManager>
    {
        [SerializeField] private GameObject _playerPuppetPrefab;

        public GameObject PlayerPuppetPrefab => _playerPuppetPrefab;

        protected override void Init()
        {
            DontDestroyOnLoad(this);
        }
    }
}
