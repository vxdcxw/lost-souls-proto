using System.Linq;
using LostSouls.Configuration;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace LostSouls.Editor.Build
{

    public class BuildPostProcess : IPostprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }

        public void OnPostprocessBuild(BuildReport report)
        {
            var config = Resources.FindObjectsOfTypeAll<ConfigurationManagerObject>().First();

            config.ActiveConfiguration = null;

            EditorUtility.SetDirty(config);
            AssetDatabase.SaveAssets();
        }
    }

}
