using System;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using LostSouls.Configuration;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using ConfigurationManager = LostSouls.Configuration.ConfigurationManagerObject;

namespace LostSouls.Editor.Build
{

    [InitializeOnLoad]
    public static class AutoBuild
    {
        // TODO - Retrieve from system environment
        private const string StandaloneClientPath = "Builds/client/standalone/lostsouls-client.x86_64";
        private const string ServerPath = "Builds/server/linux-x86_64/lostsouls-server.x86_64";
        private const string WebGLPath = "Builds/client/webgl";

        static AutoBuild()
        {
            // Register a callback when user press "Build" in Unity build window
            // This is required to update assets before the build starts
            BuildPlayerWindow.RegisterGetBuildPlayerOptionsHandler(OnBuildPlayerOptions);
        }

        [UsedImplicitly]
        [MenuItem("Build/Build Server")]
        public static void MenuItemBuildServer()
        {
            BuildServer((ConfigurationManager.Environment) EditorPrefs.GetInt(ConfigurationManager.BuildEnvironmentName));
        }

        [UsedImplicitly]
        [MenuItem("Build/Build Client (WebGL)")]
        public static void MenuItemBuildWebGL()
        {
            BuildWebGL((ConfigurationManager.Environment) EditorPrefs.GetInt(ConfigurationManager.BuildEnvironmentName));
        }

        [MenuItem("Build/Build Client (standalone)")]
        [UsedImplicitly]
        public static void MenuItemBuildStandaloneClient()
        {
            BuildStandaloneClient((ConfigurationManager.Environment) EditorPrefs.GetInt(ConfigurationManager.BuildEnvironmentName));
        }

        private static BuildPlayerOptions OnBuildPlayerOptions(BuildPlayerOptions buildOptions)
        {
            var environment = (ConfigurationManager.Environment) EditorPrefs.GetInt(ConfigurationManager.BuildEnvironmentName);
            var configuration = ConfigurationManager.PrepareConfigurationObject(environment);

            BuildPlayerWindow.DefaultBuildMethods.GetBuildPlayerOptions(buildOptions);

            buildOptions.targetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;

            if (EditorUserBuildSettings.allowDebugging)
                buildOptions.options |= BuildOptions.AllowDebugging;

            if (EditorUserBuildSettings.buildScriptsOnly)
                buildOptions.options |= BuildOptions.BuildScriptsOnly;

            if (EditorUserBuildSettings.buildWithDeepProfilingSupport)
                buildOptions.options |= BuildOptions.EnableDeepProfilingSupport;

            if (EditorUserBuildSettings.connectProfiler)
                buildOptions.options |= BuildOptions.ConnectWithProfiler;

            if (EditorUserBuildSettings.enableHeadlessMode)
                buildOptions.options |= BuildOptions.EnableHeadlessMode;

            if (EditorUserBuildSettings.development)
                buildOptions.options |= BuildOptions.Development;

            if (configuration.Debug != buildOptions.options.HasFlag(BuildOptions.AllowDebugging))
                Debug.LogWarning("Player build options discards 'Debug' flag from current configuration");

            var buildTarget = EditorUserBuildSettings.activeBuildTarget;

            SetPlayerOptions(ref buildOptions, buildTarget, EditorUserBuildSettings.GetBuildLocation(buildTarget));

            return buildOptions;
        }

        private static void BuildServer(ConfigurationManager.Environment environment = ConfigurationManager.Environment.None)
        {
            var configuration = ConfigurationManager.PrepareConfigurationObject(environment);
            var buildOptions = new BuildPlayerOptions();

            buildOptions.options |= BuildOptions.EnableHeadlessMode;

            SetPlayerOptions(ref buildOptions, BuildTarget.StandaloneLinux64, ServerPath, configuration);
            Build(buildOptions);
        }

        private static void BuildWebGL(ConfigurationManager.Environment environment = ConfigurationManager.Environment.None)
        {
            var configuration = ConfigurationManager.PrepareConfigurationObject(environment);
            var buildOptions = new BuildPlayerOptions();

            SetPlayerOptions(ref buildOptions, BuildTarget.WebGL, WebGLPath, configuration);
            Build(buildOptions);
        }

        private static void BuildStandaloneClient(ConfigurationManager.Environment environment = ConfigurationManager.Environment.None)
        {
            var configuration = ConfigurationManager.PrepareConfigurationObject(environment);
            var buildOptions = new BuildPlayerOptions();

            SetPlayerOptions(ref buildOptions, BuildTarget.StandaloneLinux64, StandaloneClientPath, configuration);
            Build(buildOptions);
        }

        [UsedImplicitly]
        private static void BuildServerProduction()
        {
            BuildTargetPlatformWithEnvironment(TargetPlatform.LinuxServer, ConfigurationManager.Environment.Production);
        }

        [UsedImplicitly]
        private static void BuildServerDevelopment()
        {
            BuildTargetPlatformWithEnvironment(TargetPlatform.LinuxServer, ConfigurationManager.Environment.Development);
        }

        [UsedImplicitly]
        private static void BuildWebGLProduction()
        {
            BuildTargetPlatformWithEnvironment(TargetPlatform.WebGL, ConfigurationManager.Environment.Production);
        }

        [UsedImplicitly]
        private static void BuildWebGLDevelopment()
        {
            BuildTargetPlatformWithEnvironment(TargetPlatform.WebGL, ConfigurationManager.Environment.Development);
        }

        private static void BuildTargetPlatformWithEnvironment(TargetPlatform targetPlatform, ConfigurationManager.Environment environment)
        {
            switch (targetPlatform)
            {
                case TargetPlatform.None:
                    break;
                case TargetPlatform.LinuxServer:
                    BuildServer(environment);
                    break;
                case TargetPlatform.WebGL:
                    BuildWebGL(environment);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(targetPlatform), targetPlatform, null);
            }
        }

        private static void Build(BuildPlayerOptions options)
        {
            var report = BuildPipeline.BuildPlayer(options);
            var summary = report.summary;

            switch (summary.result)
            {
                case BuildResult.Succeeded:
                    Debug.Log("Build succeeded");
                    break;
                case BuildResult.Failed:
                    Debug.Log($"Build failed with {summary.totalErrors} errors");
                    break;
                case BuildResult.Unknown:
                    Debug.Log($"Build stopped by unknown event");
                    break;
                case BuildResult.Cancelled:
                    Debug.Log($"Build cancelled by user");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void SetPlayerOptions(ref BuildPlayerOptions options, BuildTarget target, string path, ConfigurationObject configuration = null)
        {
            var startSceneName = options.options == BuildOptions.EnableHeadlessMode
                ? "Preload_Server"
                : "Preload_Client";

            options.scenes = EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
            options.scenes[0] = $"Assets/Scenes/{startSceneName}.unity";
            options.locationPathName = Path.Combine(Directory.GetCurrentDirectory(), path);
            options.target = target;

            if (configuration == null)
                return;

            if (configuration.Debug)
            {
                options.options |= BuildOptions.AllowDebugging;
                options.options |= BuildOptions.Development;
            }
        }

        private enum TargetPlatform
        {
            None,
            LinuxServer,
            WebGL,
        }
    }

}
