using System.Collections;
using Community;
using UnityEngine;

namespace LostSouls.Audio
{

    public class InGameMusic : MonoSingleton<InGameMusic>
    {
        public AudioClip TitleClip;
        public AudioClip VictoryClip;
        public AudioClip DefeatClip;
        public GameObject HurryUpPatchSFX;

        private AudioSource _source;
        private AudioClip _mainTheme;
        private AudioClip _hurryUpTheme;

        protected override void Init()
        {
            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        private void PlayClip(AudioClip clip, bool loop)
        {
            if (_source.clip == clip)
                return;

            _source.Stop();
            _source.clip = clip;
            _source.loop = loop;
            _source.Play();
        }

        public void SetGameThemes(AudioClip mainTheme, AudioClip hurryUpTheme)
        {
            _mainTheme = mainTheme;
            _hurryUpTheme = hurryUpTheme;
        }

        public void PlayTitleTheme()
        {
            PlayClip(TitleClip, true);
        }

        public void PlayMainTheme()
        {
            PlayClip(_mainTheme, true);
        }

        public void PlayHurryUpTheme()
        {
            var sfxLength = Instantiate(HurryUpPatchSFX).GetComponent<EventPlayRND>().rndEvent.length;

            StartCoroutine(PlayDelayed(_hurryUpTheme, sfxLength - 3.0f));
        }

        public void PlayEndGameTheme(bool victory)
        {
            PlayClip(victory ? VictoryClip : DefeatClip, false);
        }

        private IEnumerator PlayDelayed(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds(delay);

            PlayClip(clip, true);
        }
    }

}
