using UnityEngine;

namespace LostSouls.Audio
{

    public class EventPlayRND : MonoBehaviour
    {
        public AudioClip rndEvent;          //Pour pouvoir charger un clip audio dans l inspector
        public float lowPitchRange = 0.5f;
        public float highPitchRange = 2.0f;
        public float lowVolRange = 0.2f;
        public float highVolRange = 0.6f;

        private AudioSource _audioRNDevents;

        private void Start()
        {
            _audioRNDevents = GetComponent<AudioSource>();
            _audioRNDevents.pitch = Random.Range(lowPitchRange, highPitchRange);
            _audioRNDevents.volume = Random.Range(lowVolRange, highVolRange);
            _audioRNDevents.PlayOneShot(rndEvent);

            Destroy(gameObject, rndEvent.length); // je sais pas comment faire autrement pour supprimer l objet
        }
    }

}
