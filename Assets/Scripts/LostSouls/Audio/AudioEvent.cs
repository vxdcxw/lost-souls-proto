using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace LostSouls.Audio
{

    public class AudioEvent : MonoBehaviour
    {
        public float LowPitchRange = 0.5f;
        public float HighPitchRange = 2.0f;
        public float LowVolRange = 0.2f;
        public float HighVolRange = 0.6f;
        public List<AudioClip> FootSteps;

        private AudioSource _source;

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        [UsedImplicitly]
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public void PlaySFX()
        {
            _source.pitch = Random.Range(LowPitchRange, HighPitchRange);
            _source.volume = Random.Range(LowVolRange, HighVolRange);
            _source.PlayOneShot(FootSteps[Random.Range(0, FootSteps.Count)]);
        }
    }

}
