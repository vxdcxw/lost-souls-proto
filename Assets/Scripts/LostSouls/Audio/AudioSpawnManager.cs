using Community;
using UnityEngine;

namespace LostSouls.Audio
{
    public class AudioSpawnManager : MonoSingleton<AudioSpawnManager>
    {
        public GameObject[] EventsRND;

        private const float SpawnRangeX = 45;
        private const float StartDelay = 6;
        private const float LowRepeatRate = 6;
        private const float HighRepeatRate = 15;

        private void Start()
        {
            InvokeRepeating(nameof(SpawnEvents), StartDelay, (Random.Range(LowRepeatRate, HighRepeatRate)));
        }

        private void SpawnEvents()
        {
            var spawnPosX = new Vector3(Random.Range(-SpawnRangeX, SpawnRangeX), 0, 0);
            var eventIndex = Random.Range(0, EventsRND.Length);

            Instantiate(EventsRND[eventIndex], spawnPosX, EventsRND[eventIndex].transform.rotation);
        }

        public static void Stop()
        {
            Instance.CancelInvoke();
        }
    }
}
