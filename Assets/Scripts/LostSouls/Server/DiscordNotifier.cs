using System.Collections;
using System.Text;
using LostSouls.Configuration;
using UnityEngine.Networking;

namespace LostSouls.Server
{
    public class DiscordNotifier : Notifier
    {
        private readonly string _url;
        private readonly string _instanceName;

        public DiscordNotifier(string url, string instanceName)
        {
            _url = url;
            _instanceName = instanceName;

            Active = !string.IsNullOrWhiteSpace(_url);
        }

        public override void SendNotification(string message)
        {
            if (!Active)
                return;

            ConfigurationManager.Instance.StartCoroutine(SendNotificationRequest(message, "989898"));
        }

        public override void SendNotification(string message, string color)
        {
            if (!Active)
                return;

            ConfigurationManager.Instance.StartCoroutine(SendNotificationRequest(message, color));
        }

        private IEnumerator SendNotificationRequest(string message, string colorHex)
        {
            var color = int.Parse(colorHex, System.Globalization.NumberStyles.HexNumber);
            var request = new UnityWebRequest(_url, "Post");
            var bodyRaw = Encoding.UTF8.GetBytes("{\"username\": \"Lost Souls Notifier\", \"embeds\": [{\"title\": \"" + _instanceName + "\", \"color\": " + color + ", \"description\": \"" + message + "\"}]}");

            request.certificateHandler = new ForceAcceptAll();
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            Logger.Log(request.error != null
                    ? $"Cannot send request: {request.error}"
                    : $"Request sent successfully: {request.responseCode}", Logger.Level.Debug);
        }

        private class ForceAcceptAll : CertificateHandler
        {
            protected override bool ValidateCertificate(byte[] certificateData)
            {
                return true;
            }
        }
    }
}
