namespace LostSouls.Server
{
    public class Notifier
    {
        protected bool Active;

        public virtual void SendNotification(string message)
        {
        }

        public virtual void SendNotification(string message, string color)
        {
        }
    }
}
