using System;
using System.Collections;
using System.Linq;
using LostSouls.Game;
using LostSouls.Network;
using UnityEngine;
using UnityEngine.Networking;
#if UNITY_SERVER
using LostSouls.Configuration;
#endif

namespace LostSouls.Server
{
    public class GameManager : NetworkBehaviour
    {
        [SerializeField] private uint Seed;
        [SerializeField] private float PlayerCloseRadius = 1.75f;

        private string _gameId;
        private Level _level;

        private int _startCountdownDuration;
        private int _gameDuration;
        private int _hurryUpDuration;
        private int _shutdownDelay;
        private int _requiredPlayers;
        private int _playersConnected;
        private Coroutine _playerWaitingCoroutine;

        private static GameManager Instance { get; set; }

        public static string GameId => Instance._gameId;
        public GameMetatada Meta { get; set; }

        // TODO
        private LayerMask _playersMask;
        private Collider2D[] _colliders = new Collider2D[0];
        private NetworkTransform[] _netTransforms = new NetworkTransform[0];

        private Notifier _publicNotifier;

        private void Awake()
        {
            Instance = this;

            _playersMask = LayerMask.GetMask("Players");

#if UNITY_SERVER
            _startCountdownDuration = ConfigurationManager.Instance.CurrentConfig.StartCountdownDuration;
            _gameDuration = ConfigurationManager.Instance.CurrentConfig.GameDuration;
            _hurryUpDuration = ConfigurationManager.Instance.CurrentConfig.HurryUpDuration;
            _shutdownDelay = ConfigurationManager.Instance.CurrentConfig.ShutdownDelay;
            _requiredPlayers = ConfigurationManager.Instance.CurrentConfig.RequiredPlayers;
            _publicNotifier = new DiscordNotifier(ConfigurationManager.Instance.CurrentConfig.DiscordPublicWebhookUrl, ConfigurationManager.Instance.CurrentConfig.InstanceDisplayName);

            var chestsCount = ConfigurationManager.Instance.CurrentConfig.ChestCount;
            var minWidth = ConfigurationManager.Instance.CurrentConfig.MazeWidthMin;
            var maxWidth = ConfigurationManager.Instance.CurrentConfig.MazeWidthMax;
            var minHeight = ConfigurationManager.Instance.CurrentConfig.MazeHeightMin;
            var maxHeight = ConfigurationManager.Instance.CurrentConfig.MazeHeightMax;
            var minPathLength = ConfigurationManager.Instance.CurrentConfig.MazePathMin;
            var maxPathLength = ConfigurationManager.Instance.CurrentConfig.MazePathMax;

            var go = new GameObject();

            _level = new Level(new Vector2Int(minWidth, maxWidth),
                new Vector2Int(minHeight, maxHeight),
                new Vector2Int(minPathLength, maxPathLength),
                chestsCount,
                NetworkManager.singleton.maxConnections,
                go.transform,
                Seed,
                true,
                Logger.Log);
#endif
        }

        public override void OnStartServer()
        {
            _gameId = Guid.NewGuid().ToString();
            _playersConnected = 0;

            CustomNetworkManager.Instance.onPlayerAdd = null;
            CustomNetworkManager.Instance.onPlayerAdd += OnPlayerAdd;
            CustomNetworkManager.Instance.onDisconnect = null;
            CustomNetworkManager.Instance.onDisconnect += OnPlayerDisconnect;
            NetworkData.Instance.Timer = _gameDuration;
            NetworkData.Instance.StartCountdown = _startCountdownDuration;

            Logger.Log($"New game ({_gameId}) (connected players: {NetworkManager.singleton.numPlayers})", Logger.Level.Info, sendNotification: true);

            _level.Generate();

            Meta = new GameMetatada
            {
                InstanceName = "unknown",
                Version = Application.version,
                Id = _gameId,
                Status = "created",
                PlayersConnectedAtStart = 0,
                PlayersConnectedAtEnd = 0,
                Duration = _gameDuration,
                CreatedAt = DateTime.Now,
                StartedAt = DateTime.Now,
                EndedAt = DateTime.Now,
                Seed = _level.Seed,
                Width = _level.Width,
                Height = _level.Height,
                PathLength = _level.PathLength,
                ChestsCount = _level.ChestsCount
            };

#if UNITY_SERVER
            Meta.InstanceName = ConfigurationManager.Instance.CurrentConfig.InstanceName;
#endif

            base.OnStartServer();
        }

        private bool OnPlayerAdd(int numPlayer)
        {
            _playersConnected++;

            if (_playersConnected < _requiredPlayers)
            {
                _playerWaitingCoroutine = StartCoroutine(PlayerWaiting(1.0f));
            }

            if (NetworkData.Instance.CurrentGameState < NetworkData.GameState.StartCountdown && _playersConnected >= _requiredPlayers)
            {
                if (_playerWaitingCoroutine != null)
                {
                    StopCoroutine(_playerWaitingCoroutine);

                    _publicNotifier.SendNotification($"A game will start in {NetworkData.Instance.StartCountdown} seconds", "5ac18e");

                    _playerWaitingCoroutine = null;
                }

                StartCoroutine(StartCountdown());
            }

            return NetworkData.Instance.StartCountdown > 0;
        }

        private IEnumerator PlayerWaiting(float seconds)
        {
            NetworkData.Instance.CurrentGameState = NetworkData.GameState.WaitingPlayers;

            yield return new WaitForSeconds(seconds);

            _publicNotifier.SendNotification("A player is waiting for other players", "ffa500");
        }

        private void OnPlayerDisconnect(int numPlayer)
        {
            _playersConnected--;

            if (_playersConnected < _requiredPlayers)
            {
                NetworkData.Instance.CurrentGameState = NetworkData.GameState.NotEnoughPlayers;

                StartCoroutine(StopGame(_playersConnected < 1 ? 0 : Instance._shutdownDelay));
            }
        }

        private void Update()
        {
            if (NetworkData.Instance.CurrentGameState < NetworkData.GameState.Started || NetworkData.Instance.CurrentGameState > NetworkData.GameState.HurryUp)
                return;

            foreach (var t in _netTransforms)
            {
                if (t == null)
                    continue;

                var collisionsCount = Physics2D.OverlapCircleNonAlloc(t.targetSyncPosition, PlayerCloseRadius, _colliders, _playersMask);

                // A player is alone
                if (collisionsCount < 2)
                    break;

                // The player is not alone, but not enough players around him
                if (collisionsCount < _netTransforms.Length)
                    continue;

                // TODO - raycast other players to know if they are close enough
                NetworkData.Instance.CurrentGameState = NetworkData.GameState.Victory;

                break;
            }
        }

        private IEnumerator StartCountdown()
        {
            Logger.Log($"Countdown starts (players: {NetworkManager.singleton.numPlayers})", Logger.Level.Info);

            NetworkData.Instance.StartCountdown = _startCountdownDuration;
            NetworkData.Instance.CurrentGameState = NetworkData.GameState.StartCountdown;

            while (NetworkData.Instance.StartCountdown > 0)
            {
                yield return new WaitForSeconds(1);

                NetworkData.Instance.StartCountdown--;
            }

            _netTransforms = FindObjectsOfType<NetworkTransform>().ToArray();
            _colliders = new Collider2D[_netTransforms.Length];

            StartCoroutine(StartGameRoutine());
        }

        private IEnumerator StartGameRoutine()
        {
            Logger.Log($"Game begins (players: {NetworkManager.singleton.numPlayers})", Logger.Level.Info);

            Meta.Status = "started";
            Meta.PlayersConnectedAtStart = NetworkManager.singleton.numPlayers;
            Meta.StartedAt = DateTime.Now;

            NetworkData.Instance.CurrentGameState = NetworkData.GameState.Started;

            while (NetworkData.Instance.Timer > 0)
            {
                yield return new WaitForSeconds(1);

                NetworkData.Instance.Timer--;

                if (NetworkData.Instance.CurrentGameState >= NetworkData.GameState.Victory)
                    break;

                if (NetworkData.Instance.Timer <= _hurryUpDuration)
                    NetworkData.Instance.CurrentGameState = NetworkData.GameState.HurryUp;
            }

            if (NetworkData.Instance.CurrentGameState < NetworkData.GameState.Victory)
                NetworkData.Instance.CurrentGameState = NetworkData.GameState.GameOver;

            yield return StopGame(_shutdownDelay);
        }

        private IEnumerator StopGame(float seconds)
        {
            switch (NetworkData.Instance.CurrentGameState)
            {
                case NetworkData.GameState.Victory:
                    Logger.Log($"Victory (players: {NetworkManager.singleton.numPlayers}, remaining time: {NetworkData.Instance.Timer})", Logger.Level.Info);
                    Meta.Status = "victory";
                    break;
                case NetworkData.GameState.GameOver:
                    Logger.Log($"Time's up (players: {NetworkManager.singleton.numPlayers})", Logger.Level.Info);
                    Meta.Status = "defeat";
                    break;
                case NetworkData.GameState.NotEnoughPlayers:

                    if (_playerWaitingCoroutine != null)
                    {
                        StopCoroutine(_playerWaitingCoroutine);

                        _publicNotifier.SendNotification("Game cancelled (no more players)", "ff0033");

                        _playerWaitingCoroutine = null;
                        Meta.Status = "cancelled";
                    }
                    else
                    {
                        Logger.Log($"Not enough players (players: {NetworkManager.singleton.numPlayers}, remaining time: {NetworkData.Instance.Timer})", Logger.Level.Info);

                        Meta.Status = "not_enough_players";
                    }

                    break;
            }

            Meta.PlayersConnectedAtEnd = NetworkManager.singleton.numPlayers;
            Meta.EndedAt = DateTime.Now;

            Meta.Dump();

            yield return new WaitForSeconds(seconds);

            CustomNetworkManager.NewGame();
        }

        public class GameMetatada
        {
            public string InstanceName { get; set; }
            public string Version { get; set; }
            public string Id { get; set; }
            public string Status { get; set; }
            public int PlayersConnectedAtStart { get; set; }
            public int PlayersConnectedAtEnd { get; set; }
            public int Duration { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime StartedAt { get; set; }
            public DateTime EndedAt { get; set; }
            public uint Seed { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public int PathLength { get; set; }
            public int ChestsCount { get; set; }

            public void Dump(string fileName = "games_metadata.csv")
            {
                Logger.Log($"Writing game metadata to {fileName}", Logger.Level.Debug);
                Logger.Log($"{InstanceName};{Version};{Id};{Status};{PlayersConnectedAtStart};{PlayersConnectedAtEnd};{Duration};{CreatedAt:s};{StartedAt:s};{EndedAt:s};{Seed};{Width};{Height};{PathLength};{ChestsCount};", filename: fileName, raw: true);
            }
        }
    }
}
