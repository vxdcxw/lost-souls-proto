#if UNITY_SERVER
using System;
using System.IO;
using LostSouls.Configuration;
#endif

namespace LostSouls.Server
{
    public static class Logger
    {
        public static Level LogLevel = Level.Off;
        public static Notifier Notifier;

        public static void Log(string message, Level level = Level.Off, string fileName = null, bool sendNotification = false)
        {
            if (sendNotification)
                Notifier.SendNotification(message);

#if UNITY_SERVER
            if (level < LogLevel)
                return;

            if (fileName == null)
                fileName = $"{ConfigurationManager.Instance.CurrentConfig.InstanceName}.log";

            fileName = Path.Combine(ConfigurationManager.Instance.CurrentConfig.LogsPath, fileName);

            var now = DateTime.Now;

            using var sw = File.AppendText(fileName);

            sw.WriteLine($"[{level.ToString().ToUpper()}] {now.ToString("dd/MM/yyyy hh:mm:ss")} - {message}");
#endif
        }

        private static void RawLog(string message, string fileName, bool sendNotification = false)
        {
            if (sendNotification)
                Notifier.SendNotification(message);

#if UNITY_SERVER
            fileName = Path.Combine(ConfigurationManager.Instance.CurrentConfig.LogsPath, fileName);

            using var sw = File.AppendText(fileName);

            sw.WriteLine(message);
#endif
        }

        public static void Log(string message, int level = (int) Level.Off, string filename = null, bool raw = false, bool sendNotification = false)
        {
            if (raw)
                RawLog(message, filename, sendNotification);
            else
                Log(message, (Level) level, filename, sendNotification);
        }

        public enum Level
        {
            All,
            Debug,
            Info,
            Warning,
            Error,
            Fatal,
            Off,
        }
    }
}
