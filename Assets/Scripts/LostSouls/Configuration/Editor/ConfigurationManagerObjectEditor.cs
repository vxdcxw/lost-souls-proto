using Environment = LostSouls.Configuration.ConfigurationManagerObject.Environment;
using ConfigManager = LostSouls.Configuration.ConfigurationManagerObject;

namespace LostSouls.Configuration.Editor
{

    using UnityEditor;

    [CustomEditor(typeof(ConfigurationManagerObject))]
    public class ConfigurationManagerObjectEditor : Editor
    {
        private Environment _selectedBuildEnvironment;
        private Environment _selectedEditorEnvironment;

        private void OnEnable()
        {
            _selectedBuildEnvironment = EditorPrefs.HasKey(ConfigManager.BuildEnvironmentName) ? (Environment) EditorPrefs.GetInt(ConfigManager.BuildEnvironmentName) : Environment.Development;
            _selectedEditorEnvironment = EditorPrefs.HasKey(ConfigManager.EditorEnvironmentName) ? (Environment) EditorPrefs.GetInt(ConfigManager.EditorEnvironmentName) : Environment.Development;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUI.BeginChangeCheck();

            _selectedBuildEnvironment = (Environment) EditorGUILayout.EnumPopup(ConfigManager.BuildEnvironmentName, _selectedBuildEnvironment);
            _selectedEditorEnvironment = (Environment) EditorGUILayout.EnumPopup(ConfigManager.EditorEnvironmentName, _selectedEditorEnvironment);

            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetInt(ConfigManager.BuildEnvironmentName, (int) _selectedBuildEnvironment);
                EditorPrefs.SetInt(ConfigManager.EditorEnvironmentName, (int) _selectedEditorEnvironment);
            }
        }
    }

}
