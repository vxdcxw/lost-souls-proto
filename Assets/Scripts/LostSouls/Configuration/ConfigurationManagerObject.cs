using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace LostSouls.Configuration
{
    [CreateAssetMenu(fileName = "Configuration", menuName = "ScriptableObjects/ConfigurationManager", order = 1)]
    public class ConfigurationManagerObject : ScriptableObject
    {
        [HideInInspector] public ConfigurationObject ActiveConfiguration;

#if UNITY_EDITOR
        public const string BuildEnvironmentName = "Build Environment";
        public const string EditorEnvironmentName = "Editor Environment";

        private static bool _isBuilding;

        private static Environment _environment;

        private void OnEnable()
        {
            if (_isBuilding)
                return;

            if (ActiveConfiguration == null)
                Debug.LogWarning("No active configuration found ! Loading from editor preferences");

            if (!EditorPrefs.HasKey(EditorEnvironmentName))
            {
                Debug.LogError("No environment found in editor preferences. Please select an environment for the Configuration Manager");

                // TODO - for some reason, this does not work. Maybe because it is not an "Editor" script?
                Selection.activeObject = this;

                return;
            }

            _environment = (Environment) EditorPrefs.GetInt("Editor Environment");

            SetConfigurationObjectFromEnvironment(_environment, this);
        }

        public static ConfigurationObject PrepareConfigurationObject(Environment environment = Environment.None)
        {
            _isBuilding = true;

            var guid = AssetDatabase.FindAssets("t:" + nameof(ConfigurationManagerObject)).First();
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var configurationManager = AssetDatabase.LoadAssetAtPath<ConfigurationManagerObject>(path);

            SetConfigurationObjectFromEnvironment(environment, configurationManager);

            return configurationManager.ActiveConfiguration;
        }

        /// <summary>
        /// Set the environment to use to build the project.
        /// This method directly change project's asset.
        /// OnPreProcessBuild stage is too late and cannot be done there.
        /// </summary>
        private static void SetConfigurationObjectFromEnvironment(Environment environment, ConfigurationManagerObject configurationManager)
        {
            configurationManager.ActiveConfiguration = GetConfiguration(environment);

            EditorUtility.SetDirty(configurationManager.ActiveConfiguration);
            EditorUtility.SetDirty(configurationManager);
            AssetDatabase.SaveAssets();
        }

        private static ConfigurationObject GetConfiguration(Environment environment)
        {
            var guids = AssetDatabase.FindAssets("t:" + nameof(ConfigurationObject));

            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);

                if (path.Contains(environment.ToString()))
                    return AssetDatabase.LoadAssetAtPath<ConfigurationObject>(path);
            }

            var newPath = $"Assets/Untracked/{environment}.asset";
            var configuration = CreateInstance<ConfigurationObject>();

            Debug.LogWarning($"No ConfigurationObject found for '{environment}' environment, creating a new one at {newPath}", configuration);

            AssetDatabase.CreateAsset(configuration, newPath);

            return configuration;
        }

        public enum Environment
        {
            None,
            Production,
            Testing,
            Development,
            Local,
        }
#endif
    }
}
