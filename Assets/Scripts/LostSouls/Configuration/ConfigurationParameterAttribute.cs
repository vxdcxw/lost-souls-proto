using System;

namespace LostSouls.Configuration
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ConfigurationParameterAttribute : Attribute
    {
        private string _serializedName;

        public string SerializedName => _serializedName;

        public ConfigurationParameterAttribute(string serializedName)
        {
            _serializedName = serializedName;
        }
    }
}
