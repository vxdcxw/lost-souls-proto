using System;
using UnityEngine;
#if UNITY_SERVER
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Logger = LostSouls.Server.Logger;
#endif

namespace LostSouls.Configuration
{
    [CreateAssetMenu(fileName = "Configuration", menuName = "ScriptableObjects/Configuration", order = 1)]
    public class ConfigurationObject : ScriptableObject
    {
        [Header("Build")]
        [SerializeField] private bool _debug;

        public bool Debug => _debug;

        [Header("Application")]
        [SerializeField] private Upstream[] _remotes;

        public Upstream[] Remotes => _remotes;

#if UNITY_SERVER
        [ConfigurationParameter("instanceDisplayName")] private string _instanceDisplayName;

        public string InstanceDisplayName => _instanceDisplayName;

        [ConfigurationParameter("discordPublicWebhookUrl")] private string _discordPublicWebhookUrl;
        [ConfigurationParameter("discordPrivateWebhookUrl")] private string _discordPrivateWebhookUrl;

        public string DiscordPublicWebhookUrl => _discordPublicWebhookUrl;
        public string DiscordPrivateWebhookUrl => _discordPrivateWebhookUrl;

        [ConfigurationParameter("targetFrameRate")] private int _targetFrameRate;
        [ConfigurationParameter("useWebsockets")] private bool _useWebsockets;
        [ConfigurationParameter("maxConnections")] private int _maximumConnections;

        public int TargetFrameRate => _targetFrameRate;
        public bool UseWebsockets => _useWebsockets;
        public int MaximumConnections => _maximumConnections;

        [ConfigurationParameter("startCountdownDuration")] private int _startCountdownDuration;
        [ConfigurationParameter("gameDuration")] private int _gameDuration;
        [ConfigurationParameter("hurryUpDuration")] private int _hurryUpDuration;
        [ConfigurationParameter("shutdownDelay")] private int _shutdownDelay;
        [ConfigurationParameter("requiredPlayers")] private int _requiredPlayers;
        public int StartCountdownDuration => _startCountdownDuration;
        public int GameDuration => _gameDuration;
        public int HurryUpDuration => _hurryUpDuration;
        public int ShutdownDelay => _shutdownDelay;
        public int RequiredPlayers => _requiredPlayers;

        [ConfigurationParameter("chestCount")] private int _chestCount;
        [ConfigurationParameter("mazeWidthMin")] private int _mazeWidthMin;
        [ConfigurationParameter("mazeWidthMax")] private int _mazeWidthMax;
        [ConfigurationParameter("mazeHeightMin")] private int _mazeHeightMin;
        [ConfigurationParameter("mazeHeightMax")] private int _mazeHeightMax;
        [ConfigurationParameter("mazePathMin")] private int _mazePathMin;
        [ConfigurationParameter("mazePathMax")] private int _mazePathMax;

        public int ChestCount => _chestCount;
        public int MazeWidthMin => _mazeWidthMin;
        public int MazeWidthMax => _mazeWidthMax;
        public int MazeHeightMin => _mazeHeightMin;
        public int MazeHeightMax => _mazeHeightMax;
        public int MazePathMin => _mazePathMin;
        public int MazePathMax => _mazePathMax;

        private string _instanceName;
        private string _configPath;
        private string _logsPath;
        private LostSouls.Server.Logger.Level _logLevel;

        public string InstanceName => _instanceName;
        public string LogsPath => _logsPath;

        public void Awake()
        {
            SetConfigFromCommandLineArgs();

            if (!File.Exists(_configPath))
            {
                Application.Quit();

                throw new FileNotFoundException($"No configuration file found at path \"{_configPath}\", server will stop.");
            }

            var pairs = new Dictionary<string, string>();

            foreach (var line in File.ReadLines(_configPath))
            {
                var pair = line.Split('=');

                if (pair.Length < 2)
                    continue;

                var key = pair[0].Trim();
                var value = pair[1].Trim();

                if (pairs.ContainsKey(key))
                    pairs.Remove(key);

                pairs.Add(key, value);
            }

            var fieldInfos = GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            foreach (var fieldInfo in fieldInfos)
            {
                var attribute = fieldInfo.GetCustomAttribute(typeof(ConfigurationParameterAttribute));

                if (attribute == null) continue;

                var serializedName = ((ConfigurationParameterAttribute)attribute).SerializedName;

                if (!pairs.ContainsKey(serializedName))
                    continue;

                var value = pairs[serializedName];

                if (fieldInfo.FieldType == typeof(string))
                    fieldInfo.SetValue(this, value);
                else if (fieldInfo.FieldType == typeof(bool))
                    fieldInfo.SetValue(this, bool.Parse(value));
                else if (fieldInfo.FieldType == typeof(int))
                    fieldInfo.SetValue(this, int.Parse(value));
            }
        }

        private void SetConfigFromCommandLineArgs()
        {
            var args = Environment.GetCommandLineArgs();

            for (var i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                var nextArg1 = i < args.Length - 1 ? args[i + 1] : null;

                if (nextArg1 == null)
                    throw new ArgumentNullException();

                if (string.Compare(arg, "--name", StringComparison.Ordinal) == 0)
                {
                    _instanceName = nextArg1;

                    i++;
                }
                else if (string.Compare(arg, "--config", StringComparison.Ordinal) == 0)
                {
                    _configPath = nextArg1;

                    i++;
                }
                else if (string.Compare(arg, "--logs", StringComparison.Ordinal) == 0)
                {
                    _logsPath = nextArg1;

                    i++;
                }
                else if (string.Compare(arg, "--loglevel", StringComparison.Ordinal) == 0)
                {
                    switch (nextArg1)
                    {
                        case "all":
                            Logger.LogLevel = Logger.Level.All;
                            break;
                        case "debug":
                            Logger.LogLevel = Logger.Level.Debug;
                            break;
                        case "info":
                            Logger.LogLevel = Logger.Level.Info;
                            break;
                        case "warning":
                            Logger.LogLevel = Logger.Level.Warning;
                            break;
                        case "error":
                            Logger.LogLevel = Logger.Level.Error;
                            break;
                        case "fatal":
                            Logger.LogLevel = Logger.Level.Fatal;
                            break;
                        case "off":
                            Logger.LogLevel = Logger.Level.Off;
                            break;
                        default:
                            Logger.LogLevel = Logger.Level.Error;

                            throw new ArgumentException($"Invalid argument '{nextArg1}");
                    }

                    i++;
                }
            }
        }
#endif

        [Serializable]
        public struct Upstream
        {
            public string DisplayName;
            public string Address;
            public int Port;
            public AudioClip MainTheme;
            public AudioClip HurryUpTheme;
        }
    }
}
