using Community;
using UnityEngine;

namespace LostSouls.Configuration
{

    public class ConfigurationManager : MonoSingleton<ConfigurationManager>
    {
        [SerializeField] private ConfigurationManagerObject Manager;

        public ConfigurationObject CurrentConfig => Manager.ActiveConfiguration;

        protected override void Init()
        {
            DontDestroyOnLoad(this);
        }
    }

}
