using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Logger = LostSouls.Server.Logger;
#if UNITY_SERVER
using LostSouls.Configuration;
using LostSouls.Server;
using UnityEngine;
#endif

namespace LostSouls.Network
{

    public class CustomNetworkManager : NetworkManager
    {
        public static CustomNetworkManager Instance { get; private set; }

        public OnPlayerAdd onPlayerAdd;
        public OnDisconnect onDisconnect;
        public OnClientDisconnectEvent onClientDisconnect;

        private NetworkData _data;

        private void Awake()
        {
            // NetworkManager DontDestroyOnLoad flag does not work as expected,
            // so we have to add the flag manually
            DontDestroyOnLoad(this);

            Instance = this;

#if UNITY_WEBGL
            useWebSockets = true;
#endif

#if UNITY_SERVER
            Application.targetFrameRate = ConfigurationManager.Instance.CurrentConfig.TargetFrameRate;
            maxConnections = ConfigurationManager.Instance.CurrentConfig.MaximumConnections;
            useWebSockets = ConfigurationManager.Instance.CurrentConfig.UseWebsockets;
#endif
        }

        private void Start()
        {
#if UNITY_SERVER
            Logger.Notifier = new DiscordNotifier(ConfigurationManager.Instance.CurrentConfig.DiscordPrivateWebhookUrl, ConfigurationManager.Instance.CurrentConfig.InstanceDisplayName);

            Logger.Log($"Start network manager", Logger.Level.Info);
            Logger.Log($"Use WebSockets: {useWebSockets}", Logger.Level.Debug);
            Logger.Log($"Max connections: {maxConnections}", Logger.Level.Debug);
            Logger.Log($"Logs path: {ConfigurationManager.Instance.CurrentConfig.LogsPath}", Logger.Level.Debug);

            StartServer();
#else
            SceneManager.LoadScene(offlineScene);
#endif
        }

        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            Logger.Log($"connection {conn.connectionId} - Add player", Logger.Level.Debug);

            if (onPlayerAdd == null)
            {
                return;
            }

            if (!onPlayerAdd.Invoke(numPlayers))
            {
                Logger.Log($"connection {conn.connectionId} - Hold connection (a game is running)", Logger.Level.Debug);

                return;
            }

            base.OnServerAddPlayer(conn, playerControllerId);

            if (_data != null)
                _data.ActiveConnections = numPlayers;
        }

        public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
        {
            Logger.Log($"connection {conn.connectionId} - Remove player", Logger.Level.Debug);

            base.OnServerRemovePlayer(conn, player);

            if (_data != null)
                _data.ActiveConnections = numPlayers;
        }

        public override void OnServerConnect(NetworkConnection conn)
        {
            Logger.Log($"connection {conn.connectionId} - Connect", Logger.Level.Debug);

            base.OnServerConnect(conn);

            if (_data != null)
                _data.ActiveConnections = numPlayers;
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            Logger.Log($"connection {conn.connectionId} - Disconnect", Logger.Level.Debug);

            base.OnServerDisconnect(conn);

            if (_data != null)
                _data.ActiveConnections = numPlayers;

            onDisconnect?.Invoke(numPlayers);
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            base.OnClientDisconnect(conn);

            if (onClientDisconnect != null)
                onClientDisconnect(conn);
        }

        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            base.OnClientError(conn, errorCode);
        }

        public static void RegisterDataHolder(NetworkData data)
        {
            Instance._data = data;
        }

        public static void NewGame()
        {
            Instance.ServerChangeScene("Game");
        }

        public delegate bool OnPlayerAdd(int count);
        public delegate void OnDisconnect(int count);
        public delegate void OnClientDisconnectEvent(NetworkConnection conn);
    }

}
