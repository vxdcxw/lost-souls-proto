using System;
using JetBrains.Annotations;
using LostSouls.Game.HUD;
using UnityEngine.Events;
using UnityEngine.Networking;
#if UNITY_SERVER
using LostSouls.Configuration;
#endif

namespace LostSouls.Network
{
    public class NetworkData : NetworkBehaviour
    {
        public static NetworkData Instance;

        public UnityAction OnGameCountdown;
        public UnityAction OnGameStart;
        public UnityAction OnGameHurryUp;
        public UnityAction<bool> OnGameEnd;

        [field: SyncVar(hook = nameof(UpdateGameState))] public GameState CurrentGameState { get; set; }

        [field: SyncVar(hook = nameof(UpdateActiveConnections))] public int ActiveConnections { get; set; }
        [field: SyncVar(hook = nameof(UpdateTimer))] public int Timer { get; set; }
        [field: SyncVar(hook = nameof(UpdateCountdown))] public int StartCountdown { get; set; }

        private bool _isWaiting;

        public void Awake()
        {
            Instance = this;

            OnGameCountdown = null;
            OnGameStart = null;
            OnGameHurryUp = null;
            OnGameEnd = null;
        }

        private void Start()
        {
            CustomNetworkManager.RegisterDataHolder(this);
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            UpdateCountdown(StartCountdown);
            UpdateTimer(Timer);
            UpdateActiveConnections(ActiveConnections);
            UpdateGameState(CurrentGameState);
        }

        [UsedImplicitly]
        private void UpdateCountdown(int countdown)
        {
            HUDManager.UpdateCountdown(countdown);
        }

        [UsedImplicitly]
        private void UpdateTimer(int timer)
        {
            HUDManager.UpdateTimer(timer);
        }

        [UsedImplicitly]
        private void UpdateActiveConnections(int connections)
        {
            ActiveConnections = connections;

            HUDManager.UpdateActiveConnections(connections);
        }

        [UsedImplicitly]
        private void UpdateGameState(GameState state)
        {
            switch (state)
            {
                case GameState.NotEnoughPlayers:
                    HUDManager.DisplayLastPlayerMessage();
                    break;
                case GameState.WaitingPlayers:
                    HUDManager.DisplayWaitMessage();
                    _isWaiting = true;
                    break;
                case GameState.StartCountdown:
                    OnGameCountdown?.Invoke();
                    _isWaiting = true;
                    break;
                case GameState.Started:
                    OnGameStart?.Invoke();

                    break;
                case GameState.HurryUp:
                    OnGameHurryUp?.Invoke();
                    break;
                case GameState.Victory:
                    OnGameEnd?.Invoke(true);
                    break;
                case GameState.GameOver:
                    OnGameEnd?.Invoke(false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            if (!_isWaiting)
                HUDManager.DisplaySpectatorMessage();
        }

        public enum GameState
        {
            NotEnoughPlayers,
            WaitingPlayers,
            StartCountdown,
            Started,
            HurryUp,
            Victory,
            GameOver,
        }
    }
}
