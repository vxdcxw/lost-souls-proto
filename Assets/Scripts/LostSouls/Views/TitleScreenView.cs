using LostSouls.Audio;
using LostSouls.Configuration;
using LostSouls.Network;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace LostSouls.Views
{

    public class TitleScreenView : MonoBehaviour
    {
        [SerializeField] private GameObject _upstreamPrefab;
        [SerializeField] private Transform _upstreamList;

        public Text MessageLabel;
        [Multiline] public string MessagePressKey;
        [Multiline] public string MessagePleaseWait;
        [Multiline] public string MessageErrorServerUnreachable;

        private CustomNetworkManager _networkManager;
        private GameObject _lastSelected;
        private bool _isConnecting;

        private void Start()
        {
            _networkManager = CustomNetworkManager.Instance;

            InGameMusic.Instance.PlayTitleTheme();

            MessageLabel.text = MessagePressKey;

            FillUpstreamList();
        }

        private void Update()
        {
            ListenPlayerInput();
            KeepLastSelectedActive();
        }

        private void FillUpstreamList()
        {
            foreach (var remote in ConfigurationManager.Instance.CurrentConfig.Remotes)
            {
                var toggle = Instantiate(_upstreamPrefab, _upstreamList).GetComponent<Toggle>();

                if (_lastSelected == null)
                {
                    toggle.Select();

                    _lastSelected = toggle.gameObject;
                }

                toggle.GetComponentInChildren<Text>().text = remote.DisplayName;
                toggle.onValueChanged.AddListener(delegate
                { SelectUpstream(remote); });
            }
        }

        private void SelectUpstream(ConfigurationObject.Upstream remote)
        {
            CustomNetworkManager.Instance.networkPort = remote.Port;
            CustomNetworkManager.Instance.networkAddress = remote.Address;
            InGameMusic.Instance.SetGameThemes(remote.MainTheme, remote.HurryUpTheme);
        }

        private void GetModeFromSelectedButton()
        {
            foreach (var selectable in Selectable.allSelectablesArray)
            {
                if (EventSystem.current.currentSelectedGameObject == selectable.gameObject)
                {
                    selectable.GetComponent<Toggle>().onValueChanged.Invoke(true);
                }
            }
        }

        private void ListenPlayerInput()
        {
            if (_isConnecting)
            {
                if (Input.GetButtonUp("Cancel"))
                {
                    _networkManager.StopClient();

                    StopConnection(MessagePressKey);
                }

                return;
            }

            if (Input.GetButtonUp("Cancel"))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
#if !UNITY_WEBGL
                Application.Quit();
#endif
            }
            else if (Input.GetButtonUp("Submit"))
            {
                _isConnecting = true;
                EventSystem.current.sendNavigationEvents = false;
                MessageLabel.text = MessagePleaseWait;

                GetModeFromSelectedButton();

                _networkManager.onClientDisconnect += OnConnectionFail;
                _networkManager.StartClient();
            }
        }

        private void StopConnection(string message)
        {
            _isConnecting = false;

            EventSystem.current.sendNavigationEvents = true;

            MessageLabel.text = message;
        }

        private void OnConnectionFail(NetworkConnection conn)
        {
            StopConnection(MessageErrorServerUnreachable);
        }

        private void KeepLastSelectedActive()
        {
            if (EventSystem.current.currentSelectedGameObject != null)
                _lastSelected = EventSystem.current.currentSelectedGameObject;
            else if (_lastSelected != null)
            {
                var toggle = _lastSelected.GetComponent<Toggle>();

                if (toggle != null)
                    toggle.Select();
            }
        }
    }

}
