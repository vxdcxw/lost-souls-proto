/*
 * Author: Mordak
 */

//#define DEBUG_OUTPUT
//#define DEBUG_LOG

using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

#if DEBUG_OUTPUT
using System.IO;
#endif

namespace LostSouls.Generators
{

    public class MazeGenerator
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int _pathLength;
        private Stack<int> _gPathStack; // Pile stockant les localisation de chache élements du chemin
        private string _path;

        [Flags]
        public enum Flag
        {
            Null = 0x00,
            North = 0x01,
            South = 0x02,
            East = 0x04,
            West = 0x08,
            Noway = 0x10,
            Path = 0x20,
            End = 0x40,
            Begin = 0x80,

            Trampo = 0x0100
        };

        /* -- Public Methods -- */

        public MazeGenerator(int width, int height, int pathLength)
        {
            _width = width;
            _height = height;
            _pathLength = pathLength;
        }

        public void SetPath(string path)
        {
            _path = path;
        }

        public void Generate()
        {
            Generate(0);
        }

        public Flag[] Generate(int seed)
        {
#if DEBUG_LOG
        Debug.Log ("Methodic initalisation of Generate Labyrinth launched.");
        Debug.Log ("width=" + _width + " height=" + _height+ " path_length=" + _pathLength);
#endif
            Flag[] grid = null;
            int size;
            int i;
            var pathFounded = false;

            if (seed > 0)
                Random.InitState(seed);

            size = _width * _height;
            i = 0;
            while (!pathFounded && i < 10)
            {
                int begin;

                CreateMainPath(ref grid, (begin = InitGrid(out grid, size)), 1, ref pathFounded);

                if (pathFounded)
                {
                    var j = 0;

                    Random.InitState((int) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);       // Break determined random created by seed.
#if DEBUG_LOG
                Debug.Log("prototype generated on try " + (i + 1) + " !");
#endif
                    while (DestroyShortcut(grid, AStar(grid, begin, size), _gPathStack) && j++ < 10)
                        ;
#if DEBUG_LOG
                if (j <= 10) Debug.Log("All shortcuts has been cleaned.");
                else  Debug.LogWarning("Unable to clear all shortcuts !");
#endif
                    if (j > 10)
                        return null;

                    MarkNoway(grid, size);
                    ResolveNoway(grid, size);

#if DEBUG_OUTPUT
                WriteOnFile(grid, size);
#endif
                    return grid;
                }
                i++;
            }
#if DEBUG_LOG
        Debug.LogWarning("grid not found !");
#endif
            return null;
        }

        /* -- Private Methods -- */

        /// <summary>
        /// create main branches of the maze
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="l"></param>
        /// <param name="i"></param>
        /// <param name="pathFounded"></param>
        /// <returns></returns>
        private int CreateMainPath(ref Flag[] grid, int l, int i, ref bool pathFounded)
        {
            if (!pathFounded)
            {
                i++;
                grid[l] |= Flag.Path;
                _gPathStack.Push(l);
                if (i == _pathLength)
                {
                    pathFounded = true;
                    grid[l] |= Flag.End;
                    return 0;
                }
            }

            var tmp = (Flag) (Random.Range(1, 0x8));

            if ((grid[l] & Flag.North) != 0)
                grid[l] |= Flag.North | (Flag) ((int) tmp << 1);
            else if ((grid[l] & Flag.South) != 0)
                grid[l] |= Flag.South | (tmp & (Flag) 0x01) | (Flag) ((int) tmp << 1);
            else if ((grid[l] & Flag.East) != 0)
                grid[l] |= Flag.East | (tmp & (Flag) 0x03) | (Flag) (((int) tmp << 1) & 0x08);
            else if ((grid[l] & Flag.West) != 0)
                grid[l] |= Flag.West | tmp;
            else
                grid[l] |= (Flag) Random.Range(1, 0x10);

            var locationX = l % _width;
            var locationY = l / _width;

            // Determine this below values if you wqnt to change the form of the grid.
            var possibleNorth = (locationY != 0) ? true : false;
            var possibleSouth = (locationY != (_height - 1)) ? true : false;
            var possibleWest = (locationX != 0) ? true : false;
            var possibleEast = (locationX != (_width - 1)) ? true : false;

            grid[l] &= (possibleNorth) ? (Flag) 0xFF : (Flag) 0xFE;
            grid[l] &= (possibleSouth) ? (Flag) 0xFF : (Flag) 0xFD;
            grid[l] &= (possibleEast) ? (Flag) 0xFF : (Flag) 0xFB;
            grid[l] &= (possibleWest) ? (Flag) 0xFF : (Flag) 0xF7;

            if ((possibleNorth) && ((grid[l] & Flag.North) != 0))
            {
                if (grid[l - _width] == Flag.Null)
                {
                    grid[l - _width] |= Flag.South;
                    CreateMainPath(ref grid, l - _width, i, ref pathFounded);
                }
                else if ((grid[l - _width] & Flag.South) == Flag.South)
                { }
                else
                    grid[l] &= (Flag) 0xFE;
            }
            if ((possibleSouth) && ((grid[l] & Flag.South) != 0))
            {
                if (grid[l + _width] == Flag.Null)
                {
                    grid[l + _width] |= Flag.North;
                    CreateMainPath(ref grid, l + _width, i, ref pathFounded);
                }
                else if ((grid[l + _width] & Flag.North) == Flag.North)
                { }
                else
                    grid[l] &= (Flag) 0xFD;
            }
            if ((possibleEast) && ((grid[l] & Flag.East) != 0))
            {
                if (grid[l + 1] == Flag.Null)
                {
                    grid[l + 1] |= Flag.West;
                    CreateMainPath(ref grid, l + 1, i, ref pathFounded);
                }
                else if ((grid[l + 1] & Flag.West) == Flag.West)
                { }
                else
                    grid[l] &= (Flag) 0xFB;
            }
            if ((possibleWest) && ((grid[l] & Flag.West) != 0))
            {
                if (grid[l - 1] == Flag.Null)
                {
                    grid[l - 1] |= Flag.East;
                    CreateMainPath(ref grid, l - 1, i, ref pathFounded);
                }
                else if ((grid[l - 1] & Flag.East) == Flag.East)
                { }
                else
                    grid[l] &= (Flag) 0xF7;
            }

            if (pathFounded)
                return 0;

            _gPathStack.Pop();
            grid[l] &= (Flag) 0xDF;
            return 0;
        }

        private int[] AStar(IList<Flag> grid, int begin, int size)
        {
            var aGrid = new int[size];
            var fifo = new Queue<int>();
            var l = begin;
            var i = 1;

#if DEBUG_LOG
        Debug.Log("a*_algorythm pass.");
#endif
            fifo.Enqueue(l);

            while (fifo.Count != 0)
            {
                var j = fifo.Count;

                while (j != 0)
                {
                    l = fifo.Dequeue();
                    aGrid[l] = i;

                    if ((grid[l] & Flag.North) != 0 && (aGrid[l - _width] == 0))
                        fifo.Enqueue(l - _width);
                    if ((grid[l] & Flag.South) != 0 && (aGrid[l + _width] == 0))
                        fifo.Enqueue(l + _width);
                    if ((grid[l] & Flag.East) != 0 && (aGrid[l + 1] == 0))
                        fifo.Enqueue(l + 1);
                    if ((grid[l] & Flag.West) != 0 && (aGrid[l - 1] == 0))
                        fifo.Enqueue(l - 1);
                    j--;
                }
                i++;
            }
            return aGrid;
        }

        /// <summary>
        /// Block all shorter ways
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="aGrid"></param>
        /// <param name="gPathStack"></param>
        /// <returns></returns>
        private bool DestroyShortcut(IList<Flag> grid, IList<int> aGrid, IEnumerable<int> gPathStack)
        {
            var state = false;          // content if the a_star algorythm has worked.
            var ancestor = -1;          // previous path tile.
            var pathList = new Stack<int>(gPathStack);

            while (pathList.Count != 0)
            {
                var l = pathList.Pop();                                 // current Location.
                var child = (pathList.Count > 0) ? pathList.Peek() : (-1);  // next path tile.

                var i = l - _width;                                     // optimisation var.

                if (((grid[l] & Flag.North) == Flag.North) && (i != child) && (i != ancestor) && (aGrid[i] < aGrid[l]))
                {
                    state = true;
                    grid[l] &= (Flag) 0xFE;
                    grid[i] &= (Flag) 0xFD;
                }
                i = l + _width;
                if (((grid[l] & Flag.South) == Flag.South) && (i != child) && (i != ancestor) && (aGrid[i] < aGrid[l]))
                {
                    state = true;
                    grid[l] &= (Flag) 0xFD;
                    grid[i] &= (Flag) 0xFE;
                }
                i = l + 1;
                if (((grid[l] & Flag.East) == Flag.East) && (i != child) && (i != ancestor) && (aGrid[i] < aGrid[l]))
                {
                    state = true;
                    grid[l] &= (Flag) 0xFB;
                    grid[i] &= (Flag) 0xF7;
                }
                i = l - 1;
                if (((grid[l] & Flag.West) == Flag.West) && (i != child) && (i != ancestor) && (aGrid[i] < aGrid[l]))
                {
                    state = true;
                    grid[l] &= (Flag) 0xF7;
                    grid[i] &= (Flag) 0xFB;
                }
                ancestor = l;
            }

            return state;
        }

        /// <summary>
        /// Mark empty row with Noway Flag
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static int MarkNoway(IList<Flag> grid, int size)
        {
            var i = 0;

            while (i < size)
            {
                if (grid[i] == Flag.Null)
                    grid[i] |= Flag.Noway;

                i++;
            }
            return 0;
        }

        /// <summary>
        /// Recursive resolution of noway tiles
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="l"></param>
        /// <param name="opened"></param>
        private void RecurseNowayResolve(IList<Flag> grid, int l, ref bool opened)
        {
            var solutions = new int[4];
            var i = 0;

            var locationX = l % _width;
            var locationY = l / _width;

            // Determine this below values if you wqnt to change the form of the grid.
            var possibleNorth = (locationY != 0) ? true : false;
            var possibleSouth = (locationY != (_height - 1)) ? true : false;
            var possibleWest = (locationX != 0) ? true : false;
            var possibleEast = (locationX != (_width - 1)) ? true : false;

            if (possibleNorth && ((grid[l - _width] & Flag.Noway) == 0))
                solutions[i++] = -2;
            if (possibleSouth && ((grid[l + _width] & Flag.Noway) == 0))
                solutions[i++] = +2;
            if (possibleWest && ((grid[l - 1] & Flag.Noway) == 0))
                solutions[i++] = -1;
            if (possibleEast && ((grid[l + 1] & Flag.Noway) == 0))
                solutions[i++] = +1;

            if (i <= 0)
                return;

            i = Random.Range(0, i);

            switch (solutions[i])
            {
                case -2:
                    grid[l] |= Flag.North;
                    grid[l - _width] |= Flag.South;
                    break;
                case +2:
                    grid[l] |= Flag.South;
                    grid[l + _width] |= Flag.North;
                    break;
                case -1:
                    grid[l] |= Flag.West;
                    grid[l - 1] |= Flag.East;
                    break;
                default:
                    grid[l] |= Flag.East;
                    grid[l + 1] |= Flag.West;
                    break;
            }

            opened = true;
            grid[l] &= (Flag) 0xEF;      // Delete no_way flag
        }

        /// <summary>
        /// fill no_way tiles
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="size"></param>
        private void ResolveNoway(IList<Flag> grid, int size)
        {
            var opened = true;

            while (opened == true)
            {
                var l = 0;

                opened = false;

                while (l < size)
                {
                    if (grid[l] == Flag.Noway)
                        RecurseNowayResolve(grid, l, ref opened);

                    l++;
                }
            }
        }

        /// <summary>
        /// Init grid, stack and others
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private int InitGrid(out Flag[] grid, int size)
        {
            _gPathStack = new Stack<int>();

            var begin = Random.Range(0, size);

            grid = new Flag[size];
            grid[begin] |= Flag.Begin;

            return begin;
        }

#if DEBUG_OUTPUT
    private struct FileOut
    {
        public readonly Flag    a;
        public readonly string  b;

        public FileOut(Flag f, string s)
        {
            this.a = f;
            this.b = s;
        }
    }

    /// <summary>
    /// debug mode: Generate debug files
    /// </summary>
    /// <param name="grid"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    private void 	WriteOnFile(IList<Flag> grid, int size)
    {
        var output = "";
        const int convertTableLength = 16;

        FileOut[] convertTable =
        {
            new FileOut(Flag.North | Flag.South | Flag.West | Flag.East, 	"┼"),
            new FileOut(Flag.North | Flag.South | Flag.West, 	"┤"),
            new FileOut(Flag.North | Flag.South | Flag.East, 	"├"),
            new FileOut(Flag.North | Flag.East | Flag.West, 	"┴"),
            new FileOut(Flag.South | Flag.East | Flag.West, 	"┬"),
            new FileOut(Flag.North | Flag.South, 				"│"),
            new FileOut(Flag.North | Flag.East, 				"└"),
            new FileOut(Flag.North | Flag.West, 				"┘"),
            new FileOut(Flag.East  | Flag.West, 				"─"),
            new FileOut(Flag.South | Flag.West, 				"┐"),
            new FileOut(Flag.South | Flag.East, 				"┌"),
            new FileOut(Flag.North,								"∙"),
            new FileOut(Flag.West,								"∙"),
            new FileOut(Flag.East,								"∙"),
            new FileOut(Flag.South,								"∙"),
            new FileOut(0x00, 									" ")
        };

        var i = 0;
        var j = 0;

        while (i < size)
        {
            j = 0;

            while (j < convertTableLength)
            {
                if (grid [i] >= (Flag)0x80)
                {
                    output += "B";
                    break;
                }
                else if ((grid [i] & Flag.End) != 0)
                {
                    output += "A";
                    break;
                }
                else if ((grid [i] & Flag.Path) != 0)
                {
                    output += "x";
                    break;
                }
                else if ((grid [i] & convertTable [j].a) == convertTable [j].a)
                {
                    output += convertTable [j].b;
                    break;
                }

                j++;
            }

            i++;

            if ((i % _width) == 0)
                output += "\n";
        }

        System.IO.File.WriteAllText (_path + "/debug_road.txt", output);
        output = "";
        i = 0;
        j = 0;

        while (i < size)
        {
            j = 0;

            while (j < convertTableLength)
            {
                if (grid [i] >= (Flag)0x80)
                {
                    output += "B";
                    break;
                }
                else if ((grid [i] & Flag.End) != 0)
                {
                    output += "A";
                    break;
                }
                else if ((grid [i] & convertTable [j].a) == convertTable [j].a)
                {
                    output += convertTable [j].b;
                    break;
                }

                j++;
            }

            i++;

            if ((i % _width) == 0)
                output += "\n";
        }

        System.IO.File.WriteAllText (_path + "/debug_result.txt", output);

        var outputBinary = new byte[size];

        var k = 0;

        while (k < size)
        {
            outputBinary[k] = (byte)grid[k];
            k++;
        }

        System.IO.File.WriteAllBytes(_path + "/numeric_output.bin", outputBinary);
    }
#endif
    }

}
