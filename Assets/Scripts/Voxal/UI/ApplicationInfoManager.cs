using UnityEngine;

namespace Voxal.UI
{
    public static class ApplicationInfoManager
    {
        public static string GetStringInfo(Info info)
        {
            return info switch
            {
                Info.Version => Application.version,
                _ => string.Empty
            };
        }

        public enum Info
        {
            Version
        }
    }
}
