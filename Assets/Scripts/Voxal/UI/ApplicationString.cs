using UnityEngine;
using UnityEngine.UI;

namespace Voxal.UI
{
    [RequireComponent(typeof(Text))]
    public class ApplicationString : MonoBehaviour
    {
        [SerializeField] private ApplicationInfoManager.Info ApplicationInfo;

        private void Start()
        {
            GetComponent<Text>().text = ApplicationInfoManager.GetStringInfo(ApplicationInfo);
        }
    }
}
