# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2021-05-03
### Changed
- More accurate Discord notifications
- Use nicely formated names for notifications

### Fixed
- Sprites orientation

## [0.1.2] - 2021-04-29
### Added
- Splash screen
- Use a configuration file for the server
- Send discord notifications on important events

### Changed
- Update project to Unity 2020.3.2f1

### Fixed
- Move items management to backend
- Move victory condition management to backend

## [0.1.1] - 2021-04-09
### Added
- This CHANGELOG file
- Treasures appeared in the maze, they will give either a coin purse or an arrow
- Player is invited to press a button to join a game
- Games are limited on time, players will loose if they don't meet up on time
- Secure Websockets support

### Changed
- Use another music for title screen

### Fixed
- Fix blurry title screen

## [0.1.0] - 2021-01-31
### Added
- Initial version of the project

[0.1.3]: https://gitlab.com/voxal-games/unity/projects/lost-souls/-/tags/0.1.3
[0.1.2]: https://gitlab.com/voxal-games/unity/projects/lost-souls/-/tags/0.1.2
[0.1.1]: https://gitlab.com/voxal-games/unity/projects/lost-souls/-/tags/0.1.1
[0.1.0]: https://gitlab.com/voxal-games/unity/projects/lost-souls/-/tags/0.1.0
