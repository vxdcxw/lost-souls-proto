ARG UNITY_EDITOR_IMAGE
ARG UNITY_EDITOR_VERSION

FROM registry.gitlab.com/voxal-games/unity/docker/$UNITY_EDITOR_IMAGE:$UNITY_EDITOR_VERSION

ARG LICENSE_PATH

ARG BUILD_TARGET
ARG BUILD_METHOD

ENV BUILD_TARGET=$BUILD_TARGET
ENV BUILD_METHOD=$BUILD_METHOD

ENV UNITY_LICENSE_USERNAME=
ENV UNITY_LICENSE_PASSWORD=

WORKDIR /project

COPY . .
COPY Unity_lic.ulf /root/.local/share/unity3d/Unity/Unity_lic.ulf

CMD xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /Unity/Editor/Unity -batchmode -username $UNITY_LICENSE_USERNAME -password $UNITY_LICENSE_PASSWORD -quit -batchmode -buildTarget $BUILD_TARGET -nographics -projectPath . -executeMethod LostSouls.Editor.Build.AutoBuild.$BUILD_METHOD -logFile /dev/stdout
