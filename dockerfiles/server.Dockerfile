FROM ubuntu

EXPOSE 7777
EXPOSE 7777/udp

WORKDIR /server

COPY . /server

CMD ./lostsouls-server.x86_64
